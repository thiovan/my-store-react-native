import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet, Dimensions } from 'react-native';
import { Container, Content, Card, CardItem } from 'native-base';
import Carousel from 'react-native-snap-carousel';
import SliderEntry from '../components/SliderEntry';
import styles, { colors } from '../styles/index.style';
import { ENTRIES1, ENTRIES2 } from '../static/entries';
import { theme } from '../configs';
import { sliderWidth, itemWidth, itemWidth2 } from '../styles/SliderEntry.style';
import SearchBar from '../components/SearchBar';
import CategoryMenu from '../components/CategoryMenu';
import TopProductItem from '../components/TopProductItem';

class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
  }

  renderItem({ item, index }) {
    return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
  }

  renderBannerItem({ item, index }) {
    return <Image style={{ height: 150, width: 280, borderRadius: 8 }} source={{ uri: item.illustration }} />
  }

  renderBanner() {
    return (
      <View style={styles.exampleContainer}>
        <Carousel
          data={ENTRIES2}
          renderItem={this.renderBannerItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          inactiveSlideScale={0.95}
          inactiveSlideOpacity={1}
          enableMomentum
          activeSlideAlignment="start"
          containerCustomStyle={styles.slider}
          contentContainerCustomStyle={styles.sliderContentContainer}
          activeAnimationType="spring"
          activeAnimationOptions={{
            friction: 4,
            tension: 40
          }}
        />
      </View>
    );
  }

  renderTopProductItem({ item, index }) {
    return (
      <TopProductItem data={item} />
    );
  }

  renderTopProduct() {
    return (
      <View style={styles.exampleContainer}>
        <Carousel
          data={ENTRIES2}
          renderItem={this.renderTopProductItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth2}
          inactiveSlideScale={1}
          inactiveSlideOpacity={1}
          enableMomentum
          activeSlideAlignment="start"
          containerCustomStyle={styles.slider}
          contentContainerCustomStyle={styles.sliderContentContainer}
          activeAnimationType="spring"
          activeAnimationOptions={{
            friction: 4,
            tension: 40
          }}
        />
      </View>
    );
  }

  render() {
    return (
      <Container>
        <Content padder>
          <View style={customStyles.topSectionBackground} />

          <View style={customStyles.topSection}>
            <Image style={customStyles.logoText} tintColor={theme.color.white} resizeMode="stretch" source={require('../assets/img/logo_text.png')} />
            <TouchableOpacity>
              <Image style={customStyles.scanIcon} source={require('../assets/icon/scan.png')} />
            </TouchableOpacity>
          </View>

          <View>
            <Text style={customStyles.textMyPay}>
              Your MyPAY Balance
            </Text>
            <Text style={customStyles.textBalance}>
              Rp. 3.500.000
            </Text>
          </View>

          {this.renderBanner()}

          <SearchBar />

          <CategoryMenu />

          <Text style={customStyles.bestSalesText}>
            Produk terlaris
          </Text>

          {this.renderTopProduct()}

        </Content>
      </Container>
    );
  }
}

const customStyles = StyleSheet.create({
  topSectionBackground: {
    position: 'absolute',
    backgroundColor: theme.color.primary,
    height: 250,
    width: Dimensions.get('window').width,
  },
  topSection: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  logoText: {
    height: 30,
    width: 90
  },
  scanIcon: {
    height: 30,
    width: 30,
  },
  textMyPay: {
    color: theme.color.white,
    fontSize: 14,
    marginTop: 24,
  },
  textBalance: {
    color: theme.color.white,
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 5
  },
  bestSalesText: {
    color: theme.color.primaryText,
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 8,
    marginBottom: 8,
    marginLeft: 4,
  }
});

export default HomeScreen;
