/* eslint-disable global-require */
/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import { Image, Text, StyleSheet } from 'react-native';
import { Container } from 'native-base';
import { app, theme } from '../configs';
import { Loading } from '../components';

class SplashScreen extends Component {

  state = {
    loading: false
  };

  componentDidMount() {
    this.setState({
      loading: true
    });
    setTimeout(() => this.setState({ loading: false }), 3000);
  }

  render() {
    return (
      <Container style={styles.container}>
        <Loading overlay loading={this.state.loading} />
        <Image style={styles.logo} resizeMode="contain" source={require('../assets/img/logo.png')} />
        <Text style={styles.version}>
          {`App ${app.version}`}
        </Text>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: 220,
    height: 220
  },
  version: {
    color: theme.color.darkBlue,
    position: 'absolute',
    bottom: 0,
    marginBottom: 8,
  }
});

export default SplashScreen;
