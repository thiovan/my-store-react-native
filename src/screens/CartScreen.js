import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { Container, Content } from 'native-base';
import { theme } from '../configs';

class CartScreen extends Component {

  constructor() {
    super();
    this.state = {
      loading: false
    }
  }

  render() {
    return (
      <Container>
        <Content padder>
          <Text style={styles.title}>
            Keranjang
          </Text>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    color: theme.color.primaryText,
    fontSize: 22,
    fontWeight: 'bold'
  }
});

export default CartScreen;
