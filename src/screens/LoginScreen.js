/* eslint-disable global-require */
/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import { Alert, AsyncStorage, Image, Text, StyleSheet } from 'react-native';
import { Container, Content, Button, Form, Item, Input, Label } from 'native-base';
import axios from 'axios';
import { Loading } from '../components';
import { app, language, theme } from '../configs';

export default class LoginScreen extends Component {

  constructor() {
    super();

    this.state = {
      loading: false,
      nohp: '',
      password: ''
    }
  }

  login = () => {
    this.setState({ loading: true });

    axios.post(app.mypay_base_url + 'login', {
      nohp: this.state.nohp,
      password: this.state.password
    })
      .then((response) => {
        this.setState({ loading: false });
        if (response.status === 200) {
          AsyncStorage.setItem('userToken', response.data.result.token);
          AsyncStorage.setItem('userPhone', response.data.result.nohp);
          AsyncStorage.setItem('userName', response.data.result.nama);
          AsyncStorage.setItem('userEmail', response.data.result.email);
        } else if (response.status === 202) {
          Alert.alert(
            language.error.title.login,
            language.error.message.login
          );
        }
      })
      .catch((error) => {
        this.setState({ loading: false });
        Alert.alert(
          language.error.title.default,
          language.error.message.default
        );
      });
  }

  render() {
    const { logo, textWelcome, item, button, buttonText, textRegister, inputPassword } = styles;

    return (
      <Container>
        <Loading loading={this.state.loading} overlay />
        <Content padder>
          <Image style={logo} tintColor={theme.color.primary} resizeMode="contain" source={require('../assets/img/logo_text.png')} />

          <Text style={textWelcome}>
            Hello again,
            {'\n'}
            Welcome back
          </Text>

          <Form>
            <Item style={item} stackedLabel>
              <Label>No Handphone</Label>
              <Input
                placeholder="Your Phone Number"
                placeholderTextColor={theme.color.placeholder}
                onChangeText={(nohp) => { this.setState({ nohp }); }}
              />
            </Item>
            <Item style={item} stackedLabel>
              <Label>Password</Label>
              <Input
                style={inputPassword}
                placeholder="Your Password"
                placeholderTextColor={theme.color.placeholder}
                secureTextEntry
                onChangeText={(password) => { this.setState({ password }); }}
              />
            </Item>
          </Form>

          <Button
            style={button}
            block
            onPress={this.login}
          >
            <Text style={buttonText}>LOGIN</Text>
          </Button>
          <Text style={textRegister}>
            Belum punya akun?
          </Text>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    height: 70,
    width: 150,
    marginTop: 30
  },
  textWelcome: {
    color: theme.color.black,
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 40
  },
  button: {
    backgroundColor: theme.color.primary,
    borderRadius: theme.radius.button,
    marginTop: 24
  },
  buttonText: {
    color: theme.color.white,
  },
  textRegister: {
    textDecorationLine: 'underline',
    alignSelf: 'center',
    marginTop: 20
  },
  item: {
    marginLeft: 0
  },
  inputPassword: {
    fontSize: 17
  }
});
