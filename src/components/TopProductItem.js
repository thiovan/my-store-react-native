import React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';
import { Card, CardItem } from 'native-base';

const TopProductItem = (props) => {
  const { data } = props;

  return (
    <Card>
      <CardItem style={styles.cardItem}>
        <View style={styles.container}>
          <Image style={styles.image} resizeMode="cover" source={{ uri: data.illustration }} />
          <Text>{data.title}</Text>
        </View>
      </CardItem>
    </Card>
  );
};

const styles = StyleSheet.create({
  cardItem: {

  },
  image: {
    height: 100,
    width: undefined
  },
  container: {
    flexDirection: 'column',
    height: 180,
    width: 150,
    borderRadius: 8
  }
});

export default TopProductItem;
