import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Card, CardItem, Icon } from 'native-base';
import { theme } from '../configs';

const SearchBar = () => {
  return (
    <Card >
      <CardItem style={styles.cardItem}>
        <View style={styles.container}>
          <Icon style={styles.icon} name="ios-search" />
          <Text style={styles.text}>
            Search
          </Text>
        </View>
      </CardItem>
    </Card>
  );
}

const styles = StyleSheet.create({
  cardItem: {
    height: 40
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  icon: {
    color: theme.color.grey
  },
  text: {
    color: theme.color.grey
  }
});

export default SearchBar;
