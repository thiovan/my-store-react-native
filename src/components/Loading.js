/* eslint-disable react/destructuring-assignment */
import React from 'react';
import { ActivityIndicator, Modal, StyleSheet, View } from 'react-native';
import { theme } from '../configs';

const Loading = (props) => {
  const {
    loading,
    ...attributes
  } = props;

  return (
    <Modal
      transparent
      animationType="none"
      visible={loading}
      onRequestClose={() => console.log('Closing modal')}
    >
      <View style={[
        style.modalBackground,
        props.overlay ? style.modalOverlay : null
      ]}>
        <View style={[
          style.wrapperBackground,
          props.overlay ? style.wrapperOverlay : null
        ]}>
          <ActivityIndicator
            size={props.overlay ? 'small' : 'large'}
            animating={loading}
            color={theme.color.logo}
          />
        </View>
      </View>
    </Modal>
  );
};

const style = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  modalOverlay: {
    backgroundColor: '#00000040'
  },
  wrapperBackground: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  wrapperOverlay: {
    borderRadius: 50,
    backgroundColor: theme.color.white,
    elevation: 2,
    padding: 10,
  }
});

export default Loading;
