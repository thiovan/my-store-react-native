import React from 'react';
import { TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import { Card, CardItem } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

const CategoryMenu = () => {
  return (
    <Card>
      <CardItem>
        <Grid>
          <Row>
            <Col>
              <TouchableOpacity style={styles.container}>
                <Image style={styles.image} source={require('../assets/icon/foods.png')} />
                <Text style={styles.text}>Makanan</Text>
              </TouchableOpacity>
            </Col>
            <Col>
              <TouchableOpacity style={styles.container}>
                <Image style={styles.image} source={require('../assets/icon/minuman.png')} />
                <Text style={styles.text}>Minuman</Text>
              </TouchableOpacity>
            </Col>
            <Col>
              <TouchableOpacity style={styles.container}>
                <Image style={styles.image} source={require('../assets/icon/snack.png')} />
                <Text style={styles.text}>Snack</Text>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={{ marginTop: 16 }}>
            <Col>
              <TouchableOpacity style={styles.container}>
                <Image style={styles.image} source={require('../assets/icon/permen.png')} />
                <Text style={styles.text}>Permen</Text>
              </TouchableOpacity>
            </Col>
            <Col>
              <TouchableOpacity style={styles.container}>
                <Image style={styles.image} source={require('../assets/icon/automotive.png')} />
                <Text style={styles.text}>Jasa</Text>
              </TouchableOpacity>
            </Col>
            <Col>
              <TouchableOpacity style={styles.container}>
                <Image style={styles.image} source={require('../assets/icon/others.png')} />
                <Text style={styles.text}>Lain-Lain</Text>
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>
      </CardItem>
    </Card>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    height: 48,
    width: 48
  },
  text: {
    fontSize: 12,
    paddingTop: 4
  }
});

export default CategoryMenu;
