const app = {
  name: 'My-Store',
  version: 'v0.0.7-beta',
  base_url: __DEV__ ? 'http://192.168.23.1/mystore_v2/public/index.php/api/' : 'http://satsetbatbet.ip-dynamic.com/mystore/api/',
  mypay_base_url: __DEV__ ? 'https://satsetbatbet.com/my-pay-dev/api/' : 'https://my-pay.id/api/'
};

const theme = {
  color: {
    primary: '#1969d8',
    secondary: '#ff2b4b',
    white: '#ffffff',
    grey: '#a0a4a8',
    black: '#000000',
    placeholder: '#bbbbbb',
    darkBlue: '#34495e',
    logo: '#013C80',
    primaryText: '#25282b'
  },
  padding: 16,
  elevation: 2,
  radius: {
    button: 4
  }
};

const language = {
  error: {
    title: {
      default: 'Terjadi Kesalahan',
      login: 'Login Gagal'
    },
    message: {
      default: 'Silahkan coba beberapa saat lagi',
      login: 'No handphone atau password anda salah'
    }
  }
};

export {
  app,
  theme,
  language,
};
