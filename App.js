/* eslint-disable global-require */
import React from 'react';
import { Image } from 'react-native';
import { createAppContainer, createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import SplashScreen from './src/screens/SplashScreen';
import LoginScreen from './src/screens/LoginScreen';
import HomeScreen from './src/screens/HomeScreen';
import CartScreen from './src/screens/CartScreen';
import HistoryScreen from './src/screens/HistoryScreen';
import AccountScreen from './src/screens/AccountScreen';
import { theme } from './src/configs';

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  }
);

const CartStack = createStackNavigator(
  {
    Cart: CartScreen
  },
  {
    initialRouteName: 'Cart',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  }
);

const TabRouter = createBottomTabNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: {
        tabBarLabel: 'Home',
        tabBarIcon: ({ focused }) => (
          focused
            ? <Image style={{ width: 28, height: 28 }} resizeMode="contain" source={require('./src/assets/icon/home_active.png')} />
            : <Image style={{ width: 28, height: 28 }} resizeMode="contain" source={require('./src/assets/icon/home_inactive.png')} />
        )
      }
    },
    Cart: {
      screen: CartStack,
      navigationOptions: {
        tabBarLabel: 'Cart',
        tabBarIcon: ({ focused }) => (
          focused
            ? <Image style={{ width: 28, height: 28 }} resizeMode="contain" source={require('./src/assets/icon/cart_active.png')} />
            : <Image style={{ width: 28, height: 28 }} resizeMode="contain" source={require('./src/assets/icon/cart_inactive.png')} />
        )
      }
    },
    History: {
      screen: HistoryScreen,
      navigationOptions: {
        tabBarLabel: 'History',
        tabBarIcon: ({ focused }) => (
          focused
            ? <Image style={{ width: 28, height: 28 }} resizeMode="contain" source={require('./src/assets/icon/history_active.png')} />
            : <Image style={{ width: 28, height: 28 }} resizeMode="contain" source={require('./src/assets/icon/history_inactive.png')} />
        )
      }
    },
    Account: {
      screen: AccountScreen,
      navigationOptions: {
        tabBarLabel: 'Account',
        tabBarIcon: ({ focused }) => (
          focused
            ? <Image style={{ width: 28, height: 28 }} resizeMode="contain" source={require('./src/assets/icon/user_active.png')} />
            : <Image style={{ width: 28, height: 28 }} resizeMode="contain" source={require('./src/assets/icon/user_inactive.png')} />
        )
      }
    },
  },
  {
    tabBarOptions: {
      activeTintColor: theme.color.primary,
      labelStyle: {
        fontSize: 11,
      },
      style: {
        backgroundColor: theme.color.white,
      }
    }
  }
)

const AppNavigator = createStackNavigator(
  {
    Splash: SplashScreen,
    Login: LoginScreen,
    Main: TabRouter,
    Develop: CartScreen
  },
  {
    initialRouteName: 'Develop',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  }
);

export default createAppContainer(AppNavigator);
